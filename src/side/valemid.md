# Mõisted

> dBm– Detsibell mWkohta

> ASU = Arbitrary Strength Unit

> RSSI = Received Signal Strength Indicator

> RxLev = signaali tugevus

> RxQual = signaali kvaliteet

> D = taaskasutuse kaugus

> Time Slot = These GSM slot is the smallest individual time period that is available to each mobile. It has a defined format because a variety of different types of data are required to be transmitted.

# Teisendused

> 1mW = \\( 1 * 10^{-3} W \\) = 0.001 W
> 1 W = \\( 1 * 10^{3} W \\) = 1000 mW


### TA

`GSM Timing Advance, näitab kui kaugel asub levi kasutaja aktiivsest mastist, üks ühik näitab ligikaugu 550m-st vahemaad.`

`TA väärtus 2 näitab, et kasutaja on mastist 2-3 ühiku kaugusel, ehk 1-1.5 Km-i kaugusel.`


# 2G

\\[ S[dBm]=2*ASU-113  \\]

# 3G

\\[ D = R * 1.732  \\]

\\[ S[dBm]= ASU-115  \\]

# 4G


# Signaali võimsus

> **RMS Voltage** = 0.707

> MicroWatts to dBm

\\[ P_{dBm} = 10 * log_{10}(P_{mW} / 1mW )  \\]

\\[ P_{dBV} = 20 * log_{10}(U_{RMS} )  \\]

> dBm to MicroWatts

\\[ P_{mW} = 1mW * 10^{(P_{dBm} / 10)}  \\]



# Sageduste taaskasutamine

\\[ D = R * \sqrt{ 3K }  \\]


-------

# Erinevad ülesanded


**ÜL1**   

`Mobiilside GSM operaatorile on eraldatud 29 sageduskanalit. Keskmiselt mitut kanalit saab ühes kärjes kasutada, kui sageduste taaskasutustegur /klastri suurus on 13?`

**Arvutus:** \\( 29 / 13 = 2.23 \\)  


**ÜL2**

`Mitu dBm’i on võimsus 9,9 mW ?`

\\( dBm = 10 * log_{10}(9,9 / 1) = 9.956 dBm \\)


**ÜL3**

`Signaali võimsus on -51 dBm.
Kui suur on see võimsus vattides?`

**Arvutus**: \\( 1mW * 10^{(-51 / 10)} = 0.000007943_{mW}\\)

**Teisendus Vattideks** \\( 0.000007943_{mW} / 10^{-6} = 7.943_{W} \\)


**ÜL4**

`GSM mobiilside kärje raadius on 6 km ja sageduste taaskasutustegur on 7, milline on minimaalne kaugus kahe samu sagedusi kasutava tugijaama vahel?`

**Arvutus**: \\( 6 * \sqrt{ 3 * 7 } = 27.495 \\)

**Vastus** \\( D = 27.495 km \\)