# Summary

- [Sissejuhatus](README.md)
- [Sissejuhatus infotehnoloogiasse](infotehnoloogia/index.md)
- [Side](side/valemid.md)
    - [valemid](side/valemid.md)
- [Diskreetne matemaatika](diskreetne_matemaatika/index.md)
    - [arvu süsteem](diskreetne_matemaatika/arvu_susteemid/index.md)
        - [2-nd süsteem](diskreetne_matemaatika/arvu_susteemid/2_nd_susteem.md)
        - [8-nd süsteem](diskreetne_matemaatika/arvu_susteemid/8_nd_susteem.md)
        - [16-nd süsteem](diskreetne_matemaatika/arvu_susteemid/16_nd_susteem.md)
        - [moodul](diskreetne_matemaatika/arvu_susteemid/mod.md)
    - [loogika tehted](diskreetne_matemaatika/loogikatehted/symbols.md)
        - [sümbolid](diskreetne_matemaatika/loogikatehted/symbols.md)


       
