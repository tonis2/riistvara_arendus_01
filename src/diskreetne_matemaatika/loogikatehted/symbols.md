 # Logic Symbols 


![symbols](assets/tabel_sumbolid.png)

--------


Name: <span style="color:#2ecc71;font-size:14px;">**logical conjunction**</span> 

Used as: <span style="color:#2ecc71;font-size:14px;"> **and** </span> 


<section class="table">

<table class="symbol-table">
 <td><span class="symbol-icon">∧</span></td>
 <td><span class="symbol-icon">·</span></td>
 <td><span class="symbol-icon">&</span></td>
</table>

| X   |      Y      |  Value |
|----------|:-------------:|------:|
| 0 |  0 | 0 |
| 0 |  1  | 0 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

</section>
 
-----
Name: <span style="color:#2ecc71;font-size:14px;">**disjunction**</span> 

Used as: <span style="color:#2ecc71;font-size:14px;"> **or** </span> 


<section class="table">

<table class="symbol-table">
 <td><span class="symbol-icon">V</span></td>
 <td><span class="symbol-icon">+</span></td>
 <td><span class="symbol-icon">|</span></td>
</table>

| X   |      Y      |  Value |
|----------|:-------------:|------:|
| 0 |  0 | 0 |
| 0 |  1  | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

</section>
 
-----

Name: <span style="color:#2ecc71;font-size:14px;">**exclusive disjunction**</span> 

Used as: <span style="color:#2ecc71;font-size:14px;"> **xor** </span> 


<section class="table">

<table class="symbol-table">
 <td><span class="symbol-icon">⊕</span></td>
 <td><span class="symbol-icon">⊻</span></td>
 <td><span class="symbol-icon">≢</span></td>
</table>

| X   |      Y      |  Value |
|----------|:-------------:|------:|
| 0 |  0 | 0 |
| 0 |  1  | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |


</section>
 
-----

Name: <span style="color:#2ecc71;font-size:14px;">**implication**</span> 

Used as: <span style="color:#2ecc71;font-size:14px;"> **if then** </span> 


<section class="table">

<table class="symbol-table">
 <td><span class="symbol-icon">-></span></td>
</table>

| X   |      Y      |  Value |
|----------|:-------------:|------:|
| 0 |  0 | 1|
| 0 |  1  | 1 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |


</section>
 
-----



Name: <span style="color:#2ecc71;font-size:14px;">**negation**</span> 

Used as: <span style="color:#2ecc71;font-size:14px;"> **not** </span> 


<section class="table">

<table class="symbol-table">
 <td><span class="symbol-icon">¬</span></td>
 <td><span class="symbol-icon">˜</span></td>
 <td><span class="symbol-icon">!</span></td>
</table>

| X   |   Value |
|----------|------:|
| 0 |  1 |
| 1 | 0 |


</section>
 
-----




Name: <span style="color:#2ecc71;font-size:14px;">**material equivalence**</span> 

Used as: <span style="color:#2ecc71;font-size:14px;"> **same as** </span> 


<section class="table">

<table class="symbol-table">
 <td><span class="symbol-icon">⇔</span></td>
 <td><span class="symbol-icon">≡</span></td>
 <td><span class="symbol-icon">↔</span></td>
 <td><span class="symbol-icon">==</span></td>
</table>


| X   |      Y      |  Value |
|----------|:-------------:|------:|
| 1 |  1 | true |
| 1 | 0 | false |
| 0 | 1 | false |
| 0 | 0 | true |


</section>
 
-----

