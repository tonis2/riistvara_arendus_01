# Kaheksand süsteem

> 8-nd süsteemis saab olla arve 
> ` 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7`


Kaheksand süsteemi teisendus kümnend süsteemi.

> \\( 25_8 = 5 * 8^0 + 2 * 8^1 = 21_10\\) 

> \\( 123_8 = 3 * 8^0 + 2 * 8^1 + 1 * 8^2 = 83_{10}\\) 


## Kümnend süsteemist 8-nd süsteemi teisendamine.

Jagan kümnendarvu arvu 8-ga, kui tekib jääk korrutan jäägi 8-ga, ja viin täisarvu järgmisele reale, vastuse loen alt üles.


\\( 348_{10} \\) =

   ```
    348 / 8 = 43.5 R 0.5 * 8 = 43 R 4

    43 / 8 = 5.375 R 0.375 * 8 = 5 R 3

    5 / 8 = 0.625 R 0.625 * 8 = 0 R 5
  ```  


**Vastus** \\( 348_{10} = 534_{8}\\)



## 8-nd süsteemist 10-nd süsteemi teisendamine.

\\( 534_{8} \\) = 

\\(5 * 8_2  + 3 * 8_1 + 4 * 8_0 \\) = `320 + 24 + 4` = \\(348_{10} \\)


## 2-nd süsteemist 8-nds süsteemi teisendamine

> \\( 2_{3} = 8 \\) seega võtame 8-nd süsteemi, 3-steks osadeks 

`101011100` = `101 | 011 | 100`

> Viin tuletatud grupid `2-nd` süsteemist `10-nd` süsteemi

`101` = \\( 1 * 2_{2} + 0 * 2_{1} + 1 * 2_0 \\) = `4 + 0 + 1 = 5`

`101` = \\( 0 * 2_{2} + 1 * 2_{1} + 1 * 2_0 \\) = `0 + 2 + 1 = 3`

`101` = \\( 1 * 2_{2} + 0 * 2_{1} + 0 * 2_0 \\) = `4 + 0 + 0 = 4`

**Vastus:** `101011100` = \\( 534_8 \\)
