# Arvu süsteemid


> Igal arvusüsteemil on olemas täisarvuline alus **P**

> Arvu järel olev **indeks** on selle arvu **alus**.

> Täisosa madalaima järgu kaal suvalises arvusüsteemis on **1**

> Koma näitab kus lähevad **täisarvulised järgukaalud** üle **murdarvulisteks**.

> Suuremate järgukaaludega on kõrgemad järgud, väiksemate järgukaaludega on madalamad järgud.

> Arvujärgu **kaal** saadakse aluse astendamisel vastava täisarvuga.

Kümnend süsteem: \\[ 0_{10},1_{10},2_{10},3_{10},4_{10}...  \\]

Kaheksand süsteem: \\[ 0_{8},1_{8},2_{8},3_{8},4_{8}...  \\]

Kahend süsteem: \\[ 0_{2},1_{2},2_{8},3_{8},4_{8}...  \\]

------


Igas järgus saab olla maksimum sama palju arvu väärtust kui arvusüsteemi alus lubab.


Kui alus on **p = 10**, siis saab selles olla arve ` 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 `

Kui alus on**p = 8**, siis saab selles olla arve ` 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7`

Kui alus on **p = 2**, siis saab selles olla arve ` 0 , 1 `

Igal arvul on oma järgukaal, mis läheb paremalt poolt vasakule iga astmega suuremaks.





# Kümnend süsteem

> Arv  1024  koosneb neljast  numbrist:    `1`    `0`    `2`    `4`

\\[ 1024_{10} =  1 * 1000 + 0 * 100 + 10 * 2 + 1 * 4  \\]

> **Tüvenumbrid** =  
> Arvu  tüvenumbrid  on  arvu numbrid alates **kõrgeimast mittnullisest numbrist**
> kuni **madalaima mittenullise numbrini**.

> Arvus **0.0000`120003`000** on tüvenumbriteks **120003**.






