# 16-nd süsteem

> 16-nd süsteemis saab olla arve `1-9`-ni ja tähti `A-F`-ni;

> ` 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7, 8, 9, A, B, C, D, E, F`

A = 10
B = 11
C = 12
D = 13
E = 14
F = 15


# 16-nd süsteemist 8-nd süsteemi teisendamine.

Jagan kümnendarvu arvu 16-ga, kui tekib jääk korrutan jäägi 16-ga, ja viin täisarvu järgmisele reale, vastuse loen alt üles.


\\( 348_{10} \\) =

   ```
    348 / 16 = 21.75 R (0.75 * 16) = 21 R 12

    21 / 16 = 1.3125 R (0.3125 * 16) = 1 R 5

    1 / 16 = 0 R 1
  ```  


**Vastus** \\( 348_{10} = 15C_{16}\\)


## 16-nd süsteemist 10-nd süsteemi teisendamine.

\\( 15C_{16} \\) = 

\\(1 * 16_2  + 5 * 16_1 + 12 * 8_0 \\) = `256 + 80 + 12` = \\(348_{10} \\)



## 2-nd süsteemist 16-nds süsteemi teisendamine

> \\( 2_{4} = 8 \\) seega võtame 8-nd süsteemi, 4-steks osadeks 

`101011100` = `1 | 0101 | 1100` = `0001 | 0101 | 1100`

> Viin tuletatud grupid `2-nd` süsteemist `10-nd` süsteemi

`0001` = \\( 0 * 2_{3} + 0 * 2_{2} + 0 * 2_{1} + 1 * 2_0 \\) = `1`

`0101` = \\( 0 * 2_{3} + 1 * 2_{2} + 0 * 2_{1} + 1 * 2_0 \\) = `4 + 1 = 5`

`1100` = \\( 1 * 2_{3} + 1 * 2_{2} + 0 * 2_{1} + 0 * 2_0 \\) = `8 + 4 = 12`

**Vastus:** `101011100` = \\( 15C_{16} \\)