# Moodularvutus (Mooduli rakendamine)


Kuidas arvutada mod-i ?

Näited

Jagan `M / N` tulemuse korrutan `N`-iga seejärel lahutan `M`-ist selle tulemuse.

`250 mod 4` = \\( 250 / 4 = 62.5 => 62 * 4 = 248 => 250 - 248 = 2 \\) = `2`

`7 mod 4` = \\( 7 / 4 = 1.75 => 1 * 4 = 4 => 7 - 4 = 2 \\) = `2`

`25 mod 6` = \\( 25 / 6 = 4.2 => 4 * 6 = 24 => 25 - 24 = 1 \\) = `1` 