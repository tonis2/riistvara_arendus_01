* **Kahend süsteem -> kümnend süsteemi näide**


# Kahend süsteem

Kahendsüsteem on lihtsaim võimalik positsiooniline arvusüsteem.

Kahendsüsteemi teisenud kümnend süsteemi.

- **11011** = 1 + 2 + 0 + 8 + 16 = **27**
- **1011** = 1 + 2 + 0 + 16 = **19**
- **1111110** = 0 + 2 + 4 + 8 + 16 + 32 + 64 = **126**


Alustad paremalt ja liigud iga numbriga ruutvõrra suuremaks

**Näiteks**

> -  **11011** = 1 + 2 + 0 + 8 + 16 = **27**
>  - **1011** = 1 + 2 + 0 + 16 = **19**
>  - **1111110** = 0 + 2 + 4 + 8 + 16 + 32 + 64 = **126**


* **Kümnend süsteem -> kahend süsteem näide**

> Jagad arvu kahega, kui jagamisel **tekib jääk**, lisad kahendsüsteemi **1**

> Kui jagamise tulemus on **täis arv** lisad kahendsüsteemi **0**

> Jagamist teed alati täis arvust.

> Teed jagamist niikaua kuni jõuad jagamisel 0-ni.

> Vastuse pöörad ringi


**Näide:** Leian arvust  \\( 103_{10} \\) kahendsüsteemi vaste.

Esimene tehe: \\[ 103 / 2 =  51.5  \\]

Kuna `51.5` on murdarv lisan kahendsüsteemi numbri **1** ja jätkan jagamist `51`-ga

Teine tehe: \\[ 51 / 2 =  25.5  \\]

Kuna `25.5` on murdarv lisan kahendsüsteemi numbri **1** ja jätkan jagamist `25`-ga

Kolmas tehe: \\[ 25 / 2 =  12.5  \\]

Kuna `12.5` on murdarv lisan kahendsüsteemi numbri **1** ja jätkan jagamist `12`-ga

Neljas tehe: \\[ 12 / 2 =  6  \\]

Kuna `6` on täisarv lisan kahendsüsteemi numbri **0** ja jätkan jagamist `6`-ga

Neljas tehe: \\[ 6 / 2 =  3  \\]

Kuna `3` on täisarv lisan kahendsüsteemi numbri **0** ja jätkan jagamist `3`-ga

Viies tehe: \\[ 3 / 2 =  1.5  \\]

Kuna `1.5` on murdarv lisan kahendsüsteemi numbri **1** ja jätkan jagamist `1`-ga

Kuues tehe: \\[ 1 / 2 =  0.5  \\]

Kuna `0.5` on murdarv lisan kahendsüsteemi numbri **1** ja lõpetan jagamised, sest peaksin jagama nulliga.

Vastuseks tuleb `1110011`, mille pöörame ringi ja saame lõpliku vastuse

\\[ 1100111_{2}  \\]


